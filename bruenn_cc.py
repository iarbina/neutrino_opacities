#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import logistic as FD_dist
import scipy.constants as sc

codata = sc.physical_constants

GF0 = codata['Fermi coupling constant'][0] * 1e-6 # [MeV^-2]
G = 8.957e-44
gv = 1.0
ga = 1.27
amu = codata['atomic mass constant'][0] * 1e3 # [g]
hbarc = codata['reduced Planck constant times c in MeV fm'][0] * 1e-13
mn = codata['neutron mass energy equivalent in MeV'][0]
mp = codata['proton mass energy equivalent in MeV'][0]
me = codata['electron mass energy equivalent in MeV'][0]
mm = codata['muon mass energy equivalent in MeV'][0]
ccm = codata['speed of light in vacuum'][0]*100

def fexp(x):
    expmax = 1.02400E+002
    return np.exp( np.minimum(expmax, np.maximum(-expmax,x)) )

def fermid(x):
    return 1/(fexp(x)+1)
    #return FD_dist.sf(x)

def opacity(rho,T,Yp,E,m,mu,phin,phip):
    x = (E-mu)/T
    return (hbarc * GF0)**2 / np.pi * (gv**2 + 3*ga**2) * E**2 * np.sqrt(1 - (m/E)**2) \
    * (1-fermid(x)) * rho / amu * (1-2*Yp) / (1-np.exp((phip-phin)/T))
    #* (1-FD_dist.sf(x)) * rho / amu * (1-2*Yp) / (1-np.exp((phip-phin)/T))

def emissivity(rho,T,Yp,E,m,mu,phin,phip):
    x = (E-mu)/T
    const = (hbarc * GF0)**2 / np.pi * (gv**2 + 3*ga**2)/amu
    return (hbarc * GF0)**2 / np.pi * (gv**2 + 3*ga**2) * E**2 * np.sqrt(1 - (m/E)**2) \
    * fermid(x) * rho / amu * (1-2*Yp) / (np.exp((phin-phip)/T)-1)
    #* FD_dist.sf(x) * rho / amu * (1-2*Yp) / (np.exp((phin-phip)/T)-1)

def emi_detailed_balance(opa,T,E,mueq):
    return np.exp((mueq-E)/T) * opa

def opa_detailed_balance(emi,T,E,mueq):
    return np.exp(-(mueq-E)/T) * emi

def integrate_energy(E,emi,opa,f):
    dE = np.diff(E)
    res = 0.
    for k in range(len(E)-1):
        res = res + E[k]**2 * dE[k] * (emi[k] - opa[k] * f[k])
    return res

def rate(rho,E,emi,opa,f):
    const = 2 * ccm * amu /((2*np.pi)**2*hbarc**3*rho)
    integ = integrate_energy(E,emi,opa,f)
    #print(f'  |-> const = {const}')
    #print(f'  |-> integ = {integ}')
    return  const * integ


dt = 1e-5
#temp = 6.30309E-001
#den = 6.92487E+009
#Yhat = 3.92478E-004
#Yp = (1-Yhat)/2
#mun = -1.04386E+001 + mn
#mup = -1.04386E+001 + 3.06871E+000 + mp
#Un = 7.63420E-006
#Up = -7.47022E-002

temp = 0.6571
den = 0.650E+10
Yhat = 4.908E-04-9.485E-06
Yp = (1-Yhat)/2
mun = -7.876 + mn
mup = -10.611 + mp
Un = 9.006E-06
Up = -7.226E-02

mul = 47.56883
#muhat = np.array([4.36821E+000,5.92794E+000,8.29860E+000,1.19018E+001,1.73783E+001,2.57021E+001,
#          3.83536E+001,5.75827E+001,8.68093E+001,1.31231E+002,1.98748E+002,3.01368E+002])
muhat = 2.735
mueq = mul - muhat + 1.2935


kmax = 12; Emin = 3; Emax = 300
Enu = np.array([(Emax/Emin)**(k/(kmax-1))*Emin for k in range(kmax)])
Exm = Enu + (mn - mp) + (Un - Up)
Exp = Enu - (mn - mp) - (Un - Up)

fnu = np.zeros(kmax)
fnubar = np.zeros(kmax)

#phin0 = mun - mnast - Un
#phip0 = mup - mpast - Up
#phin = -7.52501E+000
#phip = -1.09762E+001
phin = -7.876E+00
phip = -1.054E+01


# neutrino-neutron opacitites
em_nu = np.nan_to_num( emissivity(den,temp,Yp,Exm,mm,mul,phin,phip) )
em_nu[np.where(Exm < mm)] = 0
op_nu_det = opa_detailed_balance(em_nu,temp,Enu,mueq)
op_nu = np.nan_to_num( opacity(den,temp,Yp,Exm,mm,mul,phin,phip) )
op_nu[np.where(Exm < mm)] = 0
em_nu_det = emi_detailed_balance(op_nu,temp,Enu,mueq)
rat_nu = -rate(den,Enu,em_nu,op_nu,fnu)

#anti-neutrino-proton opacities
em_nubar = np.nan_to_num( emissivity(den,temp,Yp,Exp,mm,mul,phin,phip) )
em_nubar[np.where(Exp < mm)] = 0
op_nubar_det = opa_detailed_balance(em_nubar,temp,Enu,mueq)
op_nubar = np.nan_to_num( opacity(den,temp,Yp,Exp,mm,mul,phin,phip) )
op_nubar[np.where(Exp < mm)] = 0
em_nubar_det = emi_detailed_balance(op_nubar,temp,Enu,mueq)
rat_nubar = rate(den,Enu,em_nubar,op_nubar,fnubar)

np.set_printoptions(linewidth=np.inf)
print(':: Thermodynamic conditions:')
print(f'    |-> Density (g/cm^3)  = {den:.5e}')
print(f'    |-> Temperature (MeV) = {temp:.5e}')
print(f'    |-> Xn - Xp           = {Yhat}')
print(f'    |-> mun - mup         = {muhat}')
print()
print(f'  E (MeV)           em_nu          em_nubar           op_nu          op_nubar    (all in cm^-1)')
print(f'-------------------------------------------------------------------------------')
for k in range(len(Enu)):
    print(f'{  Enu[k]:.5e}      {em_nu[k]*ccm:.5e}      {em_nubar[k]*ccm:.5e}'
          f'      {op_nu[k]*ccm:.5e}      {op_nubar[k]*ccm:.5e}')
#print(f':: Enu (MeV) = {Enu}')
#print(f':: Opacity (cm^-1) = {op}')
#print(f':: Emissivity (cm^-1) = {em}')
print()
print(f':: Rate nu    (s^-1) = {rat_nu}')
print(f':: Rate nubar (s^-1) = {rat_nubar}')
print(f':: Rate total (s^-1) = {rat_nu+rat_nubar}')
print()
print(f':: dY = rate * dt')
print(f':: dt = {dt}')
print(f':: dY_nu    = {rat_nu*dt}')
print(f':: dY_nubar = {rat_nubar*dt}')
print(f':: dY_tot   = {(rat_nu+rat_nubar)*dt}')

#fig, ax = plt.subplots()
#ax.set_yscale('log')
#ax.plot(Enu,op)
#plt.show()
